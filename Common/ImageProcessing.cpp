//=============================================================================
//
// Copyright(c) 2020 Mirametrix Inc. All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Mirametrix and
// are protected by copyright law. They may not be disclosed
// to third parties or copied or duplicated in any form, in whole or
// in part, without the prior written consent of Mirametrix.
//
//=============================================================================

//=============================================================================
// I N C L U D E   F I L E S

#include "ImageProcessing.h"


//-----------------------------------------------------------------------------
//
void
ImageProcessing::ShowFacePositionStatus( cv::Mat &image, const ECIUserFramingData::FacePosStatus facePosStatus )
{
	cv::Scalar color;

	switch( facePosStatus )
	{
		case ECIUserFramingData::FacePosStatus::NORM:
			color = cv::Scalar( 0, 126, 0 );
			break;
		case ECIUserFramingData::FacePosStatus::CLOSE_TO_BORDER:
			color = cv::Scalar( 0, 255, 255 );
			break;
		case ECIUserFramingData::FacePosStatus::NOT_DETECTED:
		default:
			color = cv::Scalar( 0, 0, 255 );
			break;
	}
	if( image.size().width > 50 && image.size().height > 50 )
	{
		cv::circle( image, cv::Point( 10, 10 ), 5, color, -1 );
	}
}