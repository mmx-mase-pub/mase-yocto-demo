//=============================================================================
//
// Copyright(c) 2021 Mirametrix Inc. All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Mirametrix and
// are protected by copyright law. They may not be disclosed
// to third parties or copied or duplicated in any form, in whole or
// in part, without the prior written consent of Mirametrix.
//
//=============================================================================

#pragma once

#include <opencv2/opencv.hpp>
#include <map>
#include <vector>
#include <memory>
#include <fstream>

// macro hat trick that create a local static variable with name being "img" parameter
// variable name with line number as suffix to enfore uniquenes
#define IMG_SHOW(name , img) static DevVideoOutput img ## __LINE__ (name); img ## __LINE__ .ShowMat(img);

class DevVideoOutput
{
public :
	DevVideoOutput() = default;
	DevVideoOutput(std::string name);
	~DevVideoOutput();

	bool OpenOutput( const cv::Mat& matToShow );
	bool IsInitialized() const { return _isInitialized; }

	void ShowMat( const cv::Mat& matToShow );

private :
	bool OpenOutput( const cv::Mat& matToShow, const std::string& param );

	bool _isInitialized = false;
	std::string _name;
	std::string _writerParam;
#if defined(__linux__) 
#if defined(USE_GSTREAMER_BACKEND)
	cv::VideoWriter _cvWriter;
#elif defined(FRAME_BUFFER_DISPLAY)
	// there can be only one frame buffer at time
	// so it is shared whith all the created display instances
	// of course this implies some limitation around display
	static std::ofstream _fbStream;
	static int _fbWidth;
	static int _fbHeight;
	static int _fbBpp;
#endif
#endif

};

