//=============================================================================
//
// Copyright(c) 2021 Mirametrix Inc. All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Mirametrix and
// are protected by copyright law. They may not be disclosed
// to third parties or copied or duplicated in any form, in whole or
// in part, without the prior written consent of Mirametrix.
//
//=============================================================================

#include "DevVideoOutput.h"

#if defined( __linux__ ) && !defined( USE_GSTREAMER_BACKEND )
	#include <linux/fb.h>
	#include <unistd.h>
	#include <fcntl.h>
	#include <sys/ioctl.h>
#endif

#if defined( FRAME_BUFFER_DISPLAY ) && !defined( USE_GSTREAMER_BACKEND )

std::ofstream DevVideoOutput::_fbStream;
int DevVideoOutput::_fbWidth = 0;
int DevVideoOutput::_fbHeight = 0;
int DevVideoOutput::_fbBpp = 0;

struct framebuffer_info
{
	uint32_t bits_per_pixel;
	uint32_t xres_virtual;
	uint32_t yres_virtual;
};

struct framebuffer_info
get_framebuffer_info( const char* framebuffer_device_path )
{
	struct framebuffer_info info;
	struct fb_var_screeninfo screen_info;
	int fd = -1;
	fd = open( framebuffer_device_path, O_RDWR );
	if ( fd >= 0 )
	{
		if ( !ioctl( fd, FBIOGET_VSCREENINFO, &screen_info ) )
		{
			info.xres_virtual = screen_info.xres_virtual;
			info.yres_virtual = screen_info.yres_virtual;
			info.bits_per_pixel = screen_info.bits_per_pixel;
		}
		close( fd );
	}
	return info;
};
#endif

DevVideoOutput::DevVideoOutput( std::string name )
	: _isInitialized( false )
	, _name( name )
	, _writerParam()
#if defined( _WIN32 )
// windows we don't have to setup _cvWriter as we use "imShow" that can display image without
// any writer
#elif defined( __linux__ )
	#if defined( USE_GSTREAMER_BACKEND )
	, _cvWriter()
	#elif defined( FRAME_BUFFER_DISPLAY )
	#endif
#endif
{
}

bool
DevVideoOutput::OpenOutput( const cv::Mat& matToShow )
{
#if defined( _WIN32 )
	// this would work if windows opencv is build with gstreamer
	// but we don't offer gstreamer as backend through OpenCV on windows
	// OpenOutput( matToShow, "appsrc ! decodebin ! videoconvert ! autovideosink" );
	// there is nothing to open for ImShow
	return true;
#elif defined( __linux__ )
	#if defined( USE_GSTREAMER_BACKEND )
		// #######################################
		// Gstreamer pipeline display variants
		// #######################################
		#if defined( FRAME_BUFFER_DISPLAY )	   // GStreamer + FrameBuffer
	return OpenOutput( matToShow, "appsrc ! decodebin ! videoconvert ! fbdevsink" );
			// return OpenOutput( matToShow, "appsrc ! decodebin ! videoconvert ! fpsdisplaysink
			// silent=true video-sink=fbdevsink" );
		#else								   // GStreamer with automatic video sink
	return OpenOutput( matToShow, "appsrc ! decodebin ! videoconvert ! autovideosink" );
			// return OpenOutput( matToShow, "appsrc ! decodebin ! videoconvert ! ximagesink" );
		#endif
		// #######################################
		// END Gstreamer variants
		// #######################################
	#elif defined( FRAME_BUFFER_DISPLAY )
	// most basic possible : raw frame buffer
	return OpenOutput( matToShow, "/dev/fb0" );
	#else
	#endif
#endif
	return false;
}

bool
DevVideoOutput::OpenOutput( const cv::Mat& matToShow, const std::string& param )
{
	_isInitialized = true;
#if defined( _WIN32 )
	return true;
#elif defined( __linux__ )
	#if defined( USE_GSTREAMER_BACKEND )
	if ( !param.rfind( "appsrc", 0 ) == 0 )
	{
		std::cout << "ERROR : Gstreamer display pipeline MUST start with appsrc" << std::endl;
		return false;
	}
	std::cout << "Opening debug output " << _name.c_str()
			  << " view with parameter :" << param.c_str() << std::endl;
	if ( _cvWriter.open( _writerParam, 0, 30, cv::Size( matToShow.cols, matToShow.rows ) ) )
	{
		std::cout << "debug output " << _name.c_str() << " Opened" << std::endl;
		return true;
	}
	else
	{
		std::cout << "debug output " << _name.c_str() << " failed to open" << std::endl;
		return false;
	}
	#elif defined( FRAME_BUFFER_DISPLAY )
	// fbDesc = open( param.c_str(), O_RDWR );
	framebuffer_info fb_info = get_framebuffer_info( param.c_str() );
	_fbStream = std::ofstream( param.c_str() );
	if ( _fbStream.good() )
	{
		std::cout << "debug output " << _name.c_str() << " Opened frame buffer : " << std::endl;
		std::cout << "xres=" << fb_info.xres_virtual << std::endl;
		std::cout << "yres=" << fb_info.yres_virtual << std::endl;
		std::cout << "BitPerPixel=" << fb_info.bits_per_pixel << std::endl;

		_fbWidth = fb_info.xres_virtual;
		_fbHeight = fb_info.yres_virtual;
		_fbBpp = fb_info.bits_per_pixel;
		return true;
	}
	else
	{
		std::cout << "debug output " << _name.c_str() << " failed to open" << std::endl;
		return false;
	}
	#else
	// cv::imshow on linux, nothing to init
	return true;
	#endif
#endif
}

DevVideoOutput::~DevVideoOutput()
{
#if defined( __linux__ )
	#if defined( USE_GSTREAMER_BACKEND )

	if ( _cvWriter.isOpened() )
	{
		_cvWriter.release();
	}
	#elif defined( FRAME_BUFFER_DISPLAY )
	if ( _fbStream.good() )
	{
		_fbStream.close();
	}
	#endif
#endif
}

void
DevVideoOutput::ShowMat( const cv::Mat& matToShow )
{
	if ( !IsInitialized() )
	{
		OpenOutput( matToShow );
	}

#if defined( _WIN32 ) || \
( defined( __linux__ ) && ( !defined( USE_GSTREAMER_BACKEND ) || !defined( FRAME_BUFFER_DISPLAY ) ) )
	cv::imshow( _name.c_str(), matToShow );
	cv::waitKey( 1 );
	return;
#elif defined( __linux__ )
	#if defined( USE_GSTREAMER_BACKEND )
	if ( _cvWriter.isOpened() )
	{
		if ( matToShow.channels() == 1 )
		{
			//_cvWriter with gstreamer WANT an image with 3 channels
			cv::Mat cvout;
			std::vector<cv::Mat> channels;
			channels.push_back( matToShow );
			channels.push_back( matToShow );
			channels.push_back( matToShow );
			cv::merge( channels, cvout );
			_cvWriter.write( cvout );
		}
		else
		{
			_cvWriter.write( matToShow );
		}
		return;
	}
	#elif defined( FRAME_BUFFER_DISPLAY )
	if ( !_fbStream.good() )
	{
		return;
	}
	cv::Mat cvout;
	if ( matToShow.channels() == 1 )
	{
		//_cvWriter with gstreamer WANT an image with 3 channels
		std::vector<cv::Mat> channels;
		channels.push_back( matToShow );
		channels.push_back( matToShow );
		channels.push_back( matToShow );
		cv::merge( channels, cvout );
	}
	else
	{
		cvout = matToShow;
	}

	if ( cvout.depth() != CV_8U )
	{
		std::cerr << "Not 8 bits per pixel and channel." << std::endl;
	}
	else if ( cvout.channels() != 3 )
	{
		std::cerr << "Not 3 channels." << std::endl;
	}
	else
	{
		// 3 Channels (assumed BGR), 8 Bit per Pixel and Channel

		int framebuffer_width = _fbWidth;
		int framebuffer_depth = _fbBpp;
		cv::Size2f frame_size = cvout.size();
		cv::Mat framebuffer_compat;
		switch ( framebuffer_depth )
		{
			case 32:
			{
				std::vector<cv::Mat> split_bgr;
				cv::split( cvout, split_bgr );
				split_bgr.push_back( cv::Mat( frame_size, CV_8UC1, cv::Scalar( 255 ) ) );
				cv::merge( split_bgr, framebuffer_compat );
				for ( int y = 0; y < frame_size.height; y++ )
				{
					_fbStream.seekp( y * framebuffer_width * 4 );
					_fbStream.write( reinterpret_cast<char*>( framebuffer_compat.ptr( y ) ),
									 frame_size.width * 4 );
				}
			}
			break;
			default:
				std::cerr << "Unsupported depth of framebuffer." << std::endl;
		}
	}
	return;
	#endif
#endif
}
