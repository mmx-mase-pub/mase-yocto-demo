//=============================================================================
//
// Copyright(c) 2021 Mirametrix Inc. All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Mirametrix and
// are protected by copyright law. They may not be disclosed
// to third parties or copied or duplicated in any form, in whole or
// in part, without the prior written consent of Mirametrix.
//
//=============================================================================

#include "GazeDisplayHelpers.h"
#include <ECIEngineSettings.h>

#include <chrono>

//-----------------------------------------------------------------------------
//
void
PointTimeBasedAverager::Append( std::vector<double> points )
{
	if ( _pointListSize == 0 )
	{
		_pointListSize = points.size();
	}

	if ( points.size() != _pointListSize )
	{
		throw std::invalid_argument( "points size cannot changer over time" );
	}

	uint64_t now = std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::high_resolution_clock::now().time_since_epoch() )
		.count();
	uint64_t cutoff = now - _durationMs;
	_dataList.push_back( { now, points } );

	const auto &listBegin = _dataList.begin();
	if ( listBegin->first < cutoff )
	{
		_dataList.pop_front();
	}
}

//-----------------------------------------------------------------------------
//
std::vector<double>
PointTimeBasedAverager::Get()
{
	std::vector<double> result = std::vector<double>();
	auto listItem = _dataList.begin();
	if ( listItem == _dataList.end() )
	{
		return result;
	}
	else
	{
		result = listItem->second;
		for ( ; listItem != _dataList.end(); ++listItem )
		{
			for ( size_t index = 0; index < listItem->second.size() && index < result.size(); index++ )
			{
				result[index] += listItem->second[index];
			}
		}
		for ( size_t index = 0; index < result.size(); index++ )
		{
			result[index] /= _dataList.size();
		}
	}
	return result;
}

//-----------------------------------------------------------------------------
//
void
GazeVectorDisplay::DrawOnImage( cv::Mat &image, frame_data &frameData )
{
	ECIMaseData *maseData = frameData.maseData;
	ECIFacePoints *facePoints = frameData.facePoints;

	if ( facePoints != nullptr && facePoints->_faceValid && maseData != nullptr )
	{
		ECICameraInfo camInfo;

		if ( GetCameraInfo( &camInfo ) == ECIErrorCode::SUCCESS )
		{
			ECIPOIData uncalibratedPOG = maseData->_uncalibratedPOG;

			double camParams[3][3] =
			{ { camInfo._focalLength / camInfo._pixelSize[0], 0., camInfo._principalPoint[0] },
				{ 0., camInfo._focalLength / camInfo._pixelSize[1], camInfo._principalPoint[1] },
				{ 0., 0., 1. } };

			double *leftGaze = uncalibratedPOG._leftGaze3D;
			double *leftGazeCenter = uncalibratedPOG._leftGazeCenter3D;
			double leftGazeCL = uncalibratedPOG._POIELCL;

			double *rightGaze = uncalibratedPOG._rightGaze3D;
			double *rightGazeCenter = uncalibratedPOG._rightGazeCenter3D;
			double rightGazeCL = uncalibratedPOG._POIERCL;

			double *fusedGaze = uncalibratedPOG._fusedGaze3D;
			double *fusedGazeCenter = uncalibratedPOG._fusedGazeCenter3D;
			double fusedGazeCL = uncalibratedPOG._POIFCL;

			bool rightGlintValid = maseData->_rightEyeData._isGlintValid;
			bool leftGlintValid = maseData->_leftEyeData._isGlintValid;
			double *leftGlint2D = maseData->_leftEyeData._glint2D;
			double *rightGlint2D = maseData->_rightEyeData._glint2D;

			bool const rightGazeHasNan = std::any_of( rightGaze, rightGaze + 3u, []( double const &c )
			{
				return std::isnan( c );
			} );
			bool const leftGazeHasNan = std::any_of( leftGaze, leftGaze + 3u, []( double const &c )
			{
				return std::isnan( c );
			} );
			bool const fusedGazeHasNan = std::any_of( fusedGaze, fusedGaze + 3u, []( double const &c )
			{
				return std::isnan( c );
			} );

			if ( !rightGazeHasNan && !leftGazeHasNan && !fusedGazeHasNan )
			{
				if ( maseData->_wasFittingSuccess )
				{
					cv::Mat distParams = cv::Mat::zeros( cv::Size( 5, 1 ), CV_64FC1 );
					cv::Mat camTrans = cv::Mat::zeros( cv::Size( 3, 1 ), CV_64FC1 );
					cv::Mat camRot = cv::Mat::eye( cv::Size( 3, 3 ), CV_64FC1 );

					cv::Mat rodrigues( 3, 1, cv::DataType<double>::type );
					cv::Rodrigues( camRot, rodrigues );

					cv::Mat camParamsMatrix( 3, 3, CV_64FC1, camParams );

					if ( fusedGazeCL > 0. && _displayFusedGaze )
					{
						// filtering fuse gaze
						std::vector<double> fusedGazedAll = { fusedGaze[0], fusedGaze[1], fusedGaze[2] };
						_fusedGazeAvg.Append( fusedGazedAll );
						auto fusedGazedAvg = _fusedGazeAvg.Get();

						DrawLine( image, camRot, camTrans, camParamsMatrix, distParams, fusedGazedAvg, fusedGazeCenter );
					}
					if ( _displayLeftRightGaze )
					{
						// filtering right gaze
						std::vector<double> rightGazeAll = { rightGaze[0], rightGaze[1], rightGaze[2] };
						_rightGazeAvg.Append( rightGazeAll );
						auto rightGazeAvg = _rightGazeAvg.Get();

						// filtering left gaze
						std::vector<double> leftGazeAll = { leftGaze[0], leftGaze[1], leftGaze[2] };
						_leftGazeAvg.Append( leftGazeAll );
						auto leftGazeAvg = _leftGazeAvg.Get();

						if ( leftGazeCL > 0. )
						{
							DrawLine( image, camRot, camTrans, camParamsMatrix, distParams, leftGazeAvg, leftGazeCenter );
						}
						if ( rightGazeCL > 0. )
						{
							DrawLine( image, camRot, camTrans, camParamsMatrix, distParams, rightGazeAvg, rightGazeCenter );
						}
						if ( rightGlintValid )
						{
							cv::Point rightGlint( static_cast<int>( rightGlint2D[0] ), static_cast<int>( rightGlint2D[1] ) );
							cv::circle( image, rightGlint, 2, ( 255, 0, 255 ), cv::FILLED );
						}
						if ( leftGlintValid )
						{
							cv::Point leftGlint( static_cast<int>( leftGlint2D[0] ), static_cast<int>( leftGlint2D[1] ) );
							cv::circle( image, leftGlint, 2, ( 255, 0, 255 ), cv::FILLED );
						}
					}
				}
			}
		}
	}
}

void
GazeVectorDisplay::DrawLine( cv::Mat &image, const cv::Mat camRot, 
	const cv::Mat camTrans, const cv::Mat camParamsMatrix, const cv::Mat distParams, 
	const std::vector<double> directionGazedAvg, const double *directionGazeCenter )
{
	std::vector<cv::Point2f> directionGazeCenterIcs = ProjectPointFromCcsToIcs( directionGazeCenter, camRot, camTrans, camParamsMatrix, distParams );

	double directionGazePoints2[3];
	for ( int i = 0; i < 3; ++i )
	{
		directionGazePoints2[i] = directionGazeCenter[i] + ( directionGazedAvg[i] * _mainScale );
	}

	std::vector<cv::Point2f> directionGazeCenterIcs2 = ProjectPointFromCcsToIcs( directionGazePoints2, camRot, camTrans, camParamsMatrix, distParams );

	cv::line( image, directionGazeCenterIcs.at( 0 ), directionGazeCenterIcs2.at( 0 ), { 0., 255., 255. }, 2 );
}

//-----------------------------------------------------------------------------
//
std::vector<cv::Point2f>
ProjectPointFromCcsToIcs( const double *points, const cv::Mat camRot, const cv::Mat camTrans, const cv::Mat camParamsMatrix, const cv::Mat distParams )
{
	std::vector<cv::Point2f> projectedPoints;

	std::vector<cv::Point3f> objectPoints{};
	objectPoints.push_back( cv::Point3f( static_cast<float>( points[0] ),
		static_cast<float>( points[1] ), static_cast<float>( points[2] ) ) );

	cv::projectPoints( objectPoints, camRot, camTrans, camParamsMatrix, distParams, projectedPoints );

	return projectedPoints;
}
