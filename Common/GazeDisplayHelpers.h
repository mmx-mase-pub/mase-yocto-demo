//=============================================================================
//
// Copyright(c) 2021 Mirametrix Inc. All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Mirametrix and
// are protected by copyright law. They may not be disclosed
// to third parties or copied or duplicated in any form, in whole or
// in part, without the prior written consent of Mirametrix.
//
//=============================================================================

#pragma once

//=============================================================================
// I N C L U D E   F I L E S

#include <opencv2/opencv.hpp>
#include <ECIDataStructures.h>
#include "MaseDataHandler.h"
#include <list>
#include <vector>

std::vector<cv::Point2f> ProjectPointFromCcsToIcs( const double *points, const cv::Mat camRot, const cv::Mat camTrans, const cv::Mat camParamsMatrix, const cv::Mat distParams );

class PointTimeBasedAverager
{
public:
	void Append( std::vector<double> );

	std::vector<double> Get();

private:
	// 7 / 54.0 is the basis we used for the previous
	// implementation -> 7 frames at 54fps -> ~130ms
	uint64_t _durationMs = static_cast<uint64_t>( 7.0 / 54.0 * 1000 );
	size_t _pointListSize = 0;
	std::list<std::pair<uint64_t, std::vector<double>>> _dataList;
};

class GazeVectorDisplay
{
public:
	void DrawOnImage( cv::Mat &image, frame_data &mazeData );

private:
	PointTimeBasedAverager _leftGazeAvg;
	PointTimeBasedAverager _rightGazeAvg;
	PointTimeBasedAverager _fusedGazeAvg;
	bool _displayFusedGaze{ false };
	bool _displayLeftRightGaze{ true };
	int _mainScale{ 20 };
	void DrawLine( cv::Mat &image, const cv::Mat camRot, const cv::Mat camTrans, const cv::Mat camParamsMatrix, const cv::Mat distParams, const std::vector<double> directionGazedAvg, const double *directionGazeCenter );
};
