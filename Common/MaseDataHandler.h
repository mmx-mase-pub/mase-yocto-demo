﻿//=============================================================================
//
// Copyright(c) 2020 Mirametrix Inc. All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Mirametrix and
// are protected by copyright law. They may not be disclosed
// to third parties or copied or duplicated in any form, in whole or
// in part, without the prior written consent of Mirametrix.
//
//=============================================================================
#pragma once

#include <ECIDataStructures.h>
#include <iostream>
#include <iomanip>
#include <set>
#include <list>
#include <map>
#include <mutex>
#include <queue>
#include <atomic>
#include <opencv2/opencv.hpp>

#ifdef WIN32
#include <windows.h>
#else
#include <thread>
#include <condition_variable>
#endif


// simple struct to store data we want
struct landmarks_data
{
	bool received = false;
	bool faceValid;
	std::list<double> landmarks;
};

struct bg_mask_data
{
	int frameID;
	int computedID;
	cv::Mat mask;
};

struct frame_data
{
	double depth;
	landmarks_data landMarksData;
	ECIMaseData *maseData;
	ECIFacePoints *facePoints;
};


class MaseDataHandler
{
private:
	MaseDataHandler();
	MaseDataHandler( MaseDataHandler const & ) = delete;
	MaseDataHandler( MaseDataHandler && ) = default;
	MaseDataHandler &operator=( MaseDataHandler const & ) = delete;
	MaseDataHandler &operator=( MaseDataHandler && ) = default;
	~MaseDataHandler();

public:
	static MaseDataHandler *GetInstance();
	static void CleanInstance();
	void AttachToCallbacks();
	// main engine data callbacks
	void MaseDataCB( ECIMaseData *data );
	void FacePointDataCB( ECIFacePoints *data );
	void ImageDataCB( ECIImageData *data );
	void DetectedFaceCB( ECIDetectedFaces *data );
	void UserFramingCB( ECIUserFramingData *data );
	void ProcessedImageCallback( ECIProcessedImage* imageData );
	// MaseDataHandler controls functions
	void WriteLandmarksToJsonFile();
	void EnableEngineDataPrint( bool enable )
	{
		_dataPrint = enable;
	}
	void DisplayFinalImage( bool displayFinalImage ) 
	{ 
		_displayFinalImage = displayFinalImage; 
	}
	void EnableDataRecording( bool enable )
	{
		_dataRecording = enable;
	}
	void EnableDisplayUserFramingImage( bool enable )
	{
		_displayUserFramingImage = enable;
	}
	void EnableDisplayImage( bool enable )
	{
		_displayImage = enable;
	}
	void SetOutputDataPath( std::string path )
	{
		_outputDataPath = path;
	}
	void CheckFrameDataBufferSize();

	// the availlability of those features/data depends on engine config
	void BackgroundSegmentationCB( ECIBackgroundSegmentationData *data );
	void ObjectDetectionDataCB( ECIObjectDetectionData *data );
	void EnableBackgroundSegmentationMaskSave( bool enable );

protected:
	cv::Mat ExtractImage( int channels, int width, int height, unsigned char *data );
	void DrawLandmarksOnImage( cv::Mat &image, frame_data &frameData );
	void DrawGazeOnImage( cv::Mat &image, frame_data &frameData );

private:
	void BackgroundMaskWriteThread();
	void StopBackgroundMaskWriteThread();

	// singleton instance
	static MaseDataHandler *_sInstance;
	// enable output to console
	bool _dataPrint;
	// enable recording landmark data and saving to json with "WriteLandmarksToJsonFile"
	bool _dataRecording;
	// enable recording of background segmentation masks per frame (1 bmp file per frame).
	bool _bgSegMaskRecording;
	// enable displaying user framing output image
	bool _displayUserFramingImage;
	// enable displaying image (with landmarks and depth)
	bool _displayImage;
	//enable displaying the fully processed image (user framing and background)
	bool _displayFinalImage;
	// path used to save landmarks json and background segmentation mask images
	std::string _outputDataPath;
	// list of saved frames data
	std::map<int, frame_data> _frameDataBuffer;
	// lock to prevent access to _landmarksData between MASE callback thread 
	// and possible calls to WriteLandmarksToJsonFile from other threads
	std::mutex _dataLock;

	// objects used for writing background masks to disk
	std::vector<bg_mask_data> _backgroundMasks;
	std::mutex _backgroundMaskMutex;
	std::thread _backgroundMaskWriteThread;
	bool _runBackgroundMaskThread;
};
