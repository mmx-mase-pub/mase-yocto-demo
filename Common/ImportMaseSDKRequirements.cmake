

if (MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -WX")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /std:c++17")
else()
    set(CMAKE_CXX_STANDARD 17)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)
endif()


#set( MaseSDK_INSTALL "<Define custom Mase installation when building outside of demo folder>")
set( LibCMake_Suffix lib/cmake)

if (NOT DEFINED MaseSDK_INSTALL)
    # Value not set, we assume we are in demo folder, lookup for lib/cmake
    # in parent folder do find proper install path (may vary depending on systems)
    set( BASE_SDK_SEARCH_PATH ${CMAKE_CURRENT_LIST_DIR}/../../.. )
    if (EXISTS ${BASE_SDK_SEARCH_PATH}/${LibCMake_Suffix} )
        set( MaseSDK_INSTALL ${BASE_SDK_SEARCH_PATH} )
    elseif(EXISTS ${BASE_SDK_SEARCH_PATH}/../${LibCMake_Suffix} )
        set( MaseSDK_INSTALL ${BASE_SDK_SEARCH_PATH}/.. )
    else()
        set( MaseSDK_INSTALL /usr )
    endif()
    message(VERBOSE "Looking up for MASE in ${MaseSDK_INSTALL}/${LibCMake_Suffix}/")
endif()
find_package( MaseSDK 4.1.0 REQUIRED PATHS ${MaseSDK_INSTALL}/${LibCMake_Suffix}/ )

find_package(Threads REQUIRED)

if (NOT DEFINED OpenCV_VER)
    set(OpenCV_LookupPath "${MaseSDK_INSTALL}/lib/OpenCV/")
    if (EXISTS "${OpenCV_LookupPath}/x64-2017" )
        set( OpenCV_VER "4.5.1")
        string( REPLACE "." "" OpenCV_VER_NODOT ${OpenCV_VER} )
        set( OpenCV_LinkLibSuffix ${OpenCV_VER_NODOT} )
        link_directories( "${MaseSDK_INSTALL}/lib/OpenCV/x64-2017/lib" )
    elseif(EXISTS "${OpenCV_LookupPath}/linux64" )
        set( OpenCV_VER "4.0.1")
        set( OpenCV_LinkLibSuffix .so.${OpenCV_VER} )
        link_directories( "${MaseSDK_INSTALL}/lib" )
    elseif(EXISTS "${OpenCV_LookupPath}/linuxaarch64" )
        set( OpenCV_VER "4.5.1")
        set( OpenCV_LinkLibSuffix .so.${OpenCV_VER} )
        link_directories( "${MaseSDK_INSTALL}/lib" )
    endif()
endif()

if ( DEFINED OpenCV_VER )
    message(VERBOSE "Linking to distributed openCV version ${OpenCV_VER}")
    set( OCV_LINK_LIBS 
        opencv_core${OpenCV_LinkLibSuffix}
        opencv_highgui${OpenCV_LinkLibSuffix}
        opencv_calib3d${OpenCV_LinkLibSuffix}
        opencv_imgproc${OpenCV_LinkLibSuffix}
        opencv_video${OpenCV_LinkLibSuffix}
        opencv_videoio${OpenCV_LinkLibSuffix}
        opencv_imgcodecs${OpenCV_LinkLibSuffix}
    )
    include_directories( "${MaseSDK_INSTALL}/lib/OpenCV/include" )
else()
    # build on system where openCV is part of the OS, (Yocto/embedded)
    message(VERBOSE "Linking to System openCV")
    # Take any available version + no need for suffix thanks to linux
    # dev symlik .so 
    find_package(OpenCV REQUIRED)
    set(OCV_LINK_LIBS
        opencv_core
        opencv_highgui
        opencv_calib3d
        opencv_imgproc
        opencv_video
        opencv_videoio
        opencv_imgcodecs
    )
    include_directories( ${OpenCV_INCLUDE_DIRS} )
endif()

function( set_linux_rpath target )
if (UNIX)
    set(RPATH_BASE "\$ORIGIN;\$ORIGIN/../lib;\$LIB")
    set_property(TARGET ${target} PROPERTY INSTALL_RPATH "${RPATH_BASE}")
endif()
endfunction()

# make pdb installed to be able to run DebWithRelInfo
# pre configure debugger command to start process in msvc
function( setup_msvc_helper target )
if ( MSVC )
    install(FILES $<TARGET_PDB_FILE:${target}> DESTINATION ${MaseSDK_INSTALL}/bin OPTIONAL)
    set_property(TARGET ${target} PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "${MaseSDK_INSTALL}/bin")
    set_property(TARGET ${target} PROPERTY VS_DEBUGGER_COMMAND "${MaseSDK_INSTALL}/bin/${target}.exe")
endif()
endfunction()
