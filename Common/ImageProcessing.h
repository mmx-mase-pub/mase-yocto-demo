//=============================================================================
//
// Copyright(c) 2020 Mirametrix Inc. All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Mirametrix and
// are protected by copyright law. They may not be disclosed
// to third parties or copied or duplicated in any form, in whole or
// in part, without the prior written consent of Mirametrix.
//
//=============================================================================

#pragma once

//=============================================================================
// I N C L U D E   F I L E S

#include <opencv2/opencv.hpp>
#include <ECIDataStructures.h>


namespace ImageProcessing
{
	void ShowFacePositionStatus( cv::Mat &image, const ECIUserFramingData::FacePosStatus facePosStatus );
}
