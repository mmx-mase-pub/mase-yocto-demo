﻿//=============================================================================
//
// Copyright(c) 2020 Mirametrix Inc. All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Mirametrix and
// are protected by copyright law. They may not be disclosed
// to third parties or copied or duplicated in any form, in whole or
// in part, without the prior written consent of Mirametrix.
//
//=============================================================================


#if defined(WIN32) || defined(_WIN32) 
#define NOMINMAX
#endif

#include "ImageProcessing.h"
#include "GazeDisplayHelpers.h"
#include "DevVideoOutput.h"
#include "MaseDataHandler.h"
#include <ECIDataFlow.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <set>
#include <map>
#include <list>
#include <mutex>
#include <functional>
#include <locale>
#include <algorithm>
#include "json/json.h"
#include <opencv2/opencv.hpp>

#if defined(WIN32) || defined(_WIN32) 
#include <Windows.h>
#include <filesystem>
namespace fs = std::filesystem;
#define PATH_SEPARATOR "\\" 
#else 
#include <unistd.h>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#define PATH_SEPARATOR "/" 
#endif 


#define FRAME_DATA_BUFFER_SIZE 3600

//-----------------------------------------------------------------------------
//

// to wrap C callback to C++ functional
namespace
{
	std::function<void( ECIMaseData *data )> cppMDCallback;
	std::function<void( ECIFacePoints *data )> cppFPCallback;
	std::function<void( ECIImageData *data )> cppImageCallback;
	std::function<void( ECIDetectedFaces *data )> cppDetectedFacesCallback;
	std::function<void( ECIBackgroundSegmentationData *data )> cppBgSegCallback;
	std::function<void( ECIObjectDetectionData *data )> cppObjDetectCallback;
	std::function<void( ECIUserFramingData *data )> cppUserFramingCallback;
	std::function<void( ECIProcessedImage *data )> processedImageCallback;
	extern "C"
	{
		void MDCallbackWrapper( ECIMaseData *data )
		{
			cppMDCallback( data );
		}
		void FPCallbackWrapper( ECIFacePoints *data )
		{
			cppFPCallback( data );
		}
		void ImageCallbackWrapper( ECIImageData *data )
		{
			cppImageCallback( data );
		}
		void DetectedFacesCallbackWrapper( ECIDetectedFaces *data )
		{
			cppDetectedFacesCallback( data );
		}
		void BackgroundSegmentationCallbackWrapper( ECIBackgroundSegmentationData *data )
		{
			cppBgSegCallback( data );
		}
		void ObjectDetectionCallbackWrapper( ECIObjectDetectionData *data )
		{
			cppObjDetectCallback( data );
		}
		void UserFramingCallbackWrapper( ECIUserFramingData *data )
		{
			cppUserFramingCallback( data );
		}
		void ProcessedImageCallbackWrapper( ECIProcessedImage *data )
		{
			processedImageCallback( data );
		}
	}
}

//-----------------------------------------------------------------------------
//
// data handler singleton access
MaseDataHandler *MaseDataHandler::_sInstance = nullptr;
MaseDataHandler *MaseDataHandler::GetInstance()
{
	if ( !MaseDataHandler::_sInstance )
	{
		MaseDataHandler::_sInstance = new MaseDataHandler();
	}
	return MaseDataHandler::_sInstance;
}

void MaseDataHandler::CleanInstance()
{
	if ( MaseDataHandler::_sInstance )
	{
		delete MaseDataHandler::_sInstance;
		MaseDataHandler::_sInstance = nullptr;
	}
}

MaseDataHandler::MaseDataHandler() :
	_dataPrint{ false },
	_dataRecording{ false },
	_bgSegMaskRecording{ false },
	_displayImage{ false },
	_displayUserFramingImage{ false },
	_outputDataPath{},
	_frameDataBuffer{},
	_dataLock{},
	_backgroundMasks{},
	_backgroundMaskMutex{},
	_backgroundMaskWriteThread{},
	_runBackgroundMaskThread{ false },
	_displayFinalImage{ false }
{

}

MaseDataHandler::~MaseDataHandler()
{
	// unregister all callbacks
	RegisterFaceDataCallback( nullptr );
	RegisterFacePointsCallback( nullptr );
	RegisterImageCallback( nullptr );
	RegisterMultiUserCallback( nullptr );
	RegisterBackgroundSegmentationCallback( nullptr );
	RegisterObjectDetectionCallback( nullptr );
	RegisterUserFramingCallback( nullptr );
	RegisterProcessedImageCallback( nullptr );

	StopBackgroundMaskWriteThread();
}

// do all the necessary call to attach MASE C Callback to current instance of 
void MaseDataHandler::AttachToCallbacks()
{

	if ( !cppMDCallback )
	{
		cppMDCallback = std::bind( &MaseDataHandler::MaseDataCB, this, std::placeholders::_1 );
		RegisterFaceDataCallback( MDCallbackWrapper );
	}
	if ( !cppFPCallback )
	{
		cppFPCallback = std::bind( &MaseDataHandler::FacePointDataCB, this, std::placeholders::_1 );
		RegisterFacePointsCallback( FPCallbackWrapper );
	}
	if( !cppImageCallback && _displayImage )
	{
		cppImageCallback = std::bind( &MaseDataHandler::ImageDataCB, this, std::placeholders::_1 );
		RegisterImageCallback( ImageCallbackWrapper );
	}
	if ( !cppDetectedFacesCallback )
	{
		cppDetectedFacesCallback = std::bind( &MaseDataHandler::DetectedFaceCB, this, std::placeholders::_1 );
		RegisterMultiUserCallback( DetectedFacesCallbackWrapper );
	}
	if( !cppBgSegCallback && _bgSegMaskRecording )
	{
		cppBgSegCallback = std::bind( &MaseDataHandler::BackgroundSegmentationCB, this, std::placeholders::_1 );
		RegisterBackgroundSegmentationCallback( BackgroundSegmentationCallbackWrapper );
	}
	if ( !cppObjDetectCallback )
	{
		cppObjDetectCallback = std::bind( &MaseDataHandler::ObjectDetectionDataCB, this, std::placeholders::_1 );
		RegisterObjectDetectionCallback( ObjectDetectionCallbackWrapper );
	}
	if( !cppUserFramingCallback && _displayUserFramingImage )
	{
		cppUserFramingCallback = std::bind( &MaseDataHandler::UserFramingCB, this, std::placeholders::_1 );
		RegisterUserFramingCallback( UserFramingCallbackWrapper );
	}
	if( !processedImageCallback && _displayFinalImage )
	{
		processedImageCallback = std::bind( &MaseDataHandler::ProcessedImageCallback, this, std::placeholders::_1 );
		RegisterProcessedImageCallback( ProcessedImageCallbackWrapper );
	}
}

// This is the first callback to be called when MASE emit frame data.
// it contains all major face tracking data informations
// Others will depends on available and enabled engine features
void MaseDataHandler::MaseDataCB( ECIMaseData *data )
{
	if ( _dataPrint )
	{
		std::cout << "Frame ID " << data->_frameID << std::endl;
		std::cout << "\t_wasDetectionPerformed " << data->_wasDetectionPerformed << std::endl;
		std::cout << "\t_wasDetectionSuccess " << data->_wasDetectionSuccess << std::endl;
		std::cout << "\t_wasFittingPerformed " << data->_wasFittingPerformed << std::endl;
		std::cout << "\t_wasFittingSuccess " << data->_wasFittingSuccess << std::endl;
		std::cout << "\t_userPresent " << data->_userPresent << std::endl;
		std::cout << "\t_headOri ";
		std::cout << std::setprecision( 3 ) << data->_headOri[0] << "\t";
		std::cout << std::setprecision( 3 ) << data->_headOri[1] << "\t";
		std::cout << std::setprecision( 3 ) << data->_headOri[2] << "\t";
		std::cout << std::endl;

		std::cout << "\t_midEyeOri ";
		std::cout << std::setprecision( 3 ) << data->_midEyeOri[0] << "\t";
		std::cout << std::setprecision( 3 ) << data->_midEyeOri[1] << "\t";
		std::cout << std::setprecision( 3 ) << data->_midEyeOri[2] << "\t";
		std::cout << std::endl;
	}

	if ( _displayImage )
	{
		std::map<int, frame_data>::iterator it = _frameDataBuffer.find( data->_frameID );
		if ( it != _frameDataBuffer.end() )
		{
			it->second.depth = data->_midEyePos3D[2];
			it->second.maseData = data;
		}
		else
		{
			frame_data frameData;
			frameData.depth = data->_midEyePos3D[2];
			frameData.maseData = data;
			_frameDataBuffer[data->_frameID] = frameData;
			CheckFrameDataBufferSize();
		}
	}
}

// callback to get detected Face landmarks
void MaseDataHandler::FacePointDataCB( ECIFacePoints *data )
{
	if ( _dataPrint )
	{
		std::cout << "FacePointDataCB frameId :" << data->_frameID << std::endl;
	}

	if ( _dataRecording || _displayImage )
	{
		std::list<double> landmarks( data->_facePoints2dICS, data->_facePoints2dICS + sizeof( data->_facePoints2dICS ) / sizeof( double ) );
		std::lock_guard<std::mutex> lock( _dataLock );
		std::map<int, frame_data>::iterator it = _frameDataBuffer.find( data->_frameID );
		if ( it != _frameDataBuffer.end() )
		{
			it->second.landMarksData.received = true;
			it->second.landMarksData.faceValid = data->_faceValid;
			it->second.landMarksData.landmarks = landmarks;
			it->second.facePoints = data;
		}
		else
		{
			frame_data frameData;
			frameData.landMarksData.received = true;
			frameData.landMarksData.faceValid = data->_faceValid;
			frameData.landMarksData.landmarks = landmarks;
			frameData.facePoints = data;
			_frameDataBuffer[data->_frameID] = frameData;
			CheckFrameDataBufferSize();
		}
	}
}

// callback to get image data
void MaseDataHandler::ImageDataCB( ECIImageData *data )
{
	if ( _dataPrint )
	{
		std::cout << "ImageDataCB frameId :" << data->_frameID << std::endl;
	}

	if ( _displayImage )
	{
		cv::Mat image = ExtractImage( data->_channels, data->_width, data->_height, data->_data );

		frame_data empty{ 0 };
		frame_data &frameData = empty;
		bool frameDataFound = false;
		{
			std::lock_guard<std::mutex> lock( _dataLock );
			std::map<int, frame_data>::iterator it = _frameDataBuffer.find( data->_frameID );
			if ( it != _frameDataBuffer.end() )
			{
				frameData = _frameDataBuffer[data->_frameID];
				frameDataFound = true;
			}

			if ( frameDataFound )
			{
				//DrawLandmarksOnImage(image, frameData);
				DrawGazeOnImage( image, frameData );

			}
			IMG_SHOW( "Main Image", image )
		}
	}
}

cv::Mat MaseDataHandler::ExtractImage( int channels, int width, int height, unsigned char *data )
{
	auto cvType{ channels == 1 ? CV_8UC1 : CV_8UC3 };
	cv::Size imageSize = cv::Size( width, height );
	cv::Mat image = cv::Mat( imageSize, cvType, data );
	return image;
}

void MaseDataHandler::DrawLandmarksOnImage( cv::Mat &image, frame_data &frameData )
{
	if ( frameData.landMarksData.received && frameData.landMarksData.faceValid )
	{
		std::size_t nbPoints = frameData.landMarksData.landmarks.size() / 2;

		for ( std::list<double>::iterator it = frameData.landMarksData.landmarks.begin();
			it != frameData.landMarksData.landmarks.end(); )
		{
			int x = static_cast<int>( *( it++ ) );
			if ( it != frameData.landMarksData.landmarks.end() )
			{
				int y = static_cast<int>( *( it++ ) );
				cv::circle( image, cv::Point( x, y ), 3, ( 255, 0, 255 ), cv::FILLED );
			}
		}
	}
	double depth = frameData.depth;
	if ( depth > 0.1 && depth < 10000.0 )
	{
		std::string depthTxt = "Depth: " + std::to_string( static_cast<int>( depth ) ) + " mm";
		cv::putText( image, depthTxt, cv::Point( 50, 50 ), cv::FONT_HERSHEY_SIMPLEX, 1, ( 255, 255, 255 ) );
	}
}

void MaseDataHandler::DrawGazeOnImage( cv::Mat &image, frame_data &frameData )
{
	static GazeVectorDisplay gazeDrawer;
	// to be able to do this we need to save more than landmarks in local data
	// this need to be reworked more
	// I think std::map<int, frame_data> _frameDataBuffer; should instead be a 
	// std::map<int, ECIMaseData> _frameDataBuffer; it would have a higher cost in memory though

	gazeDrawer.DrawOnImage( image, frameData );
}

// callback to get positions of the differents face detected in frame
void MaseDataHandler::DetectedFaceCB( ECIDetectedFaces *data )
{
	if ( _dataPrint )
	{
		std::cout << "DetectedFaceCB frameId :" << data->_frameID << std::endl;
		std::cout << "Total of " << data->_nbDetectedFaces << " face(s) detected" << std::endl;
	}
}

// activate background segementation mask saving and start the thread to save to files
void MaseDataHandler::EnableBackgroundSegmentationMaskSave( bool enable )
{
	_bgSegMaskRecording = enable;
	if ( _bgSegMaskRecording )
	{
		_runBackgroundMaskThread = true;
		_backgroundMaskWriteThread = std::thread{ [=]
		{
			BackgroundMaskWriteThread();
		} };
	}
	else
	{
		StopBackgroundMaskWriteThread();
	}
}

// callback to get background segmentation mask data
void MaseDataHandler::BackgroundSegmentationCB( ECIBackgroundSegmentationData *data )
{
	if ( _dataPrint )
	{
		std::cout << "BackgroundSegmentationCB frameId :" << data->_currentFrameID << std::endl;
	}
	// ideally, data should be pushed to a queue and saved asynchronously in a 
	// similar way "_landmarksData" in order to not slow down engine callback 
	// thread by doing heavy IO operation. This is accepatble for demo 
	// purpose as long as system is not limited by slow storage
	if ( _bgSegMaskRecording )
	{
		if ( data->_backgroundSegmentationValid && data->_processFrameID >= 0 )
		{
			cv::Mat frame = ExtractImage( data->_channels, data->_width, data->_height, data->_data );
			cv::Mat saveFrame = frame.clone();

			std::lock_guard guard{ _backgroundMaskMutex };
			_backgroundMasks.emplace_back( bg_mask_data{ data->_currentFrameID, data->_processFrameID, saveFrame } );
		}
	}
}

// callback to get object detection data
void MaseDataHandler::ObjectDetectionDataCB( ECIObjectDetectionData *data )
{
	if ( _dataPrint )
	{
		std::cout << "ObjectDetectionDataCB frameId :" << data->_frameID << std::endl;
		std::cout << "Total of " << data->_nbDetectedObjects << " object(s) detected" << std::endl;
	}
}

// save data 
void MaseDataHandler::WriteLandmarksToJsonFile()
{
	if ( _dataRecording )
	{
		std::string path = ( fs::path( _outputDataPath ) / "frame_landmark.json" ).string();
		std::cout << "Stored " << _frameDataBuffer.size() << " frames. Writing to file : ";
		std::cout << path << std::endl;
		std::ofstream file;
		Json::StreamWriterBuilder builder;
		builder["commentStyle"] = "None";
		builder["indentation"] = "\t";
		std::unique_ptr<Json::StreamWriter> writer( builder.newStreamWriter() );
		Json::Value fileValue;
		{
			std::lock_guard<std::mutex> lock( _dataLock );
			for ( auto &item : _frameDataBuffer )
			{
				Json::Value jsonFrame;
				int index = 0;
				jsonFrame["frameId"] = item.first;
				if ( item.second.landMarksData.received )
				{
					jsonFrame["face_valid"] = item.second.landMarksData.faceValid;
					jsonFrame["landmarks 2D"] = Json::Value();
					for ( auto &value : item.second.landMarksData.landmarks )
					{
						jsonFrame["landmarks 2D"][index++] = value;
					}
					fileValue.append( jsonFrame );
				}
			}
		}
		file.open( path );
		if ( !file.bad() )
		{
			writer->write( fileValue, &file );
			file.close();
		}
		else
		{
			std::cout << "Failed to open output file " << path << std::endl;
		}
	}
}

// callback to get user framing image data
void MaseDataHandler::UserFramingCB( ECIUserFramingData *data )
{
	if ( _displayUserFramingImage && data->_valid )
	{
		cv::Mat image = ExtractImage( data->_channels, data->_width, data->_height, data->_data );
		IMG_SHOW( "User Framing", image )
	}
}

void MaseDataHandler::ProcessedImageCallback( ECIProcessedImage *imageData )
{
	if( _displayFinalImage && imageData->_validImage )
	{
		cv::Mat image = ExtractImage( imageData->_channels, imageData->_width, imageData->_height, imageData->_imageData );
		IMG_SHOW( "Processed Image", image )
	}
}

void MaseDataHandler::CheckFrameDataBufferSize()
{
	while ( _frameDataBuffer.size() > FRAME_DATA_BUFFER_SIZE )
	{
		_frameDataBuffer.erase( _frameDataBuffer.begin() );
	}
}

void MaseDataHandler::BackgroundMaskWriteThread()
{
	while ( _runBackgroundMaskThread || !_backgroundMasks.empty() )
	{
		std::vector<bg_mask_data> backgroundMasks;
		{
			std::lock_guard guard{ _backgroundMaskMutex };
			backgroundMasks = _backgroundMasks;
			_backgroundMasks.clear();
		}

		for ( auto const &bgMask : backgroundMasks )
		{
			// we are now outside of lock, we can take our time to write the files
			if ( bgMask.frameID >= 0 && bgMask.computedID >= 0 )
			{
				std::string frameID = std::to_string( bgMask.frameID );
				frameID.insert( 0, std::max( std::size_t( 0 ), 8 - frameID.length() ), '0' );
				std::string computedID = std::to_string( bgMask.computedID );
				computedID.insert( 0, std::max( std::size_t( 0 ), 8 - computedID.length() ), '0' );
				std::string file = "bgSegMask-" + frameID + "-" + computedID + ".bmp";
				std::string path = ( fs::path( _outputDataPath ) / file ).string();
				cv::imwrite( path.c_str(), bgMask.mask );
			}
		}

		std::this_thread::sleep_for( std::chrono::milliseconds{ 40 } );
	}
}

void MaseDataHandler::StopBackgroundMaskWriteThread()
{
	_runBackgroundMaskThread = false;
	if ( _backgroundMaskWriteThread.joinable() )
	{
		_backgroundMaskWriteThread.join();
	}
}
