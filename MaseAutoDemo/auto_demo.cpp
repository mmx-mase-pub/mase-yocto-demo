//=============================================================================
//
// Copyright(c) 2020 Mirametrix Inc. All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Mirametrix and
// are protected by copyright law. They may not be disclosed
// to third parties or copied or duplicated in any form, in whole or
// in part, without the prior written consent of Mirametrix.
//
//=============================================================================


#include <ECIDataFlow.h>
#include <ECIEngineControl.h>
#include <ECIEngineSettings.h>
#include <ECIEngineOffline.h>
#include <ECIEngineFeatures.h>
#include <ECIDataStructures.h>
#include <ECIUserFraming.h>
#include <iostream>
#include <iomanip>
#include <mutex>
#include <codecvt>
#include <locale>

#include "MaseDataHandler.h"


#ifdef WIN32
#include <windows.h>
#include <filesystem>
namespace fs = std::filesystem;
#define OS_PATH_SEP "\\"
#else
#include <unistd.h>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#define OS_PATH_SEP "/"
#endif


#define APP_NAME L"MaseDemo"

//-----------------------------------------------------------------------------
//
bool RunAndWaitMase()
{
	if ( InitializeMase() == ECIErrorCode::SUCCESS && StartMase() == ECIErrorCode::SUCCESS )
	{
		bool alive = true;
		do
		{
#ifdef WIN32
			Sleep( 5 );
#else
			usleep( 5000 );
#endif
		}
		while( IsMaseAlive( &alive ) == ECIErrorCode::SUCCESS && alive );
	}
	else
	{
		std::cout << "\n\tError when initializing engine " << std::endl;
		return false;
	}
	return true;
}

//-----------------------------------------------------------------------------
//
int main( int argc, char *argv[] )
{
	std::string inputSequencePath;
	std::string outputDataPath;

	bool haveUnknownOption = false;
	std::string unknownOptions;

	bool outputPathParamRequired = false;
	bool bgSegRequired = false;

	bool printHelp = false;
	bool doPrint = false;
	bool doRecord = false;
	bool gazeEnabled = false;
	bool isGood = true;
	bool requireDefaultPerf = false;
	bool requireMinimalPerf = false;
	bool displayMainImage = false;
	ECIFaceTrackerPerformanceMode ftPerfMode = ECIFaceTrackerPerformanceMode::NO_PROCESSING;
	size_t optind = 1;
	while( optind < argc )
	{
		std::string argValue = argv[optind];
		if( argValue == "-sequence" && optind + 1 < argc )
		{
			inputSequencePath = argv[optind + 1];
			// no avi, this is sequence folder, make sure we have trailing slash
			if( inputSequencePath.find( ".avi" ) == std::string::npos )
			{
				if( inputSequencePath.back() != *OS_PATH_SEP )
				{
					inputSequencePath += OS_PATH_SEP;
				}
			}
			outputPathParamRequired = true;
			optind += 1;
		}
		else if( argValue == "-output" && optind + 1 < argc )
		{
			outputDataPath = argv[optind + 1];
			if( outputDataPath.back() != *OS_PATH_SEP )
			{
				outputDataPath += OS_PATH_SEP;
			}
			optind += 1;
		}
		else if( argValue == "-print" )
		{
			doPrint = true;
		}
		else if( argValue == "-savelandmarks" )
		{
			doRecord = true;
			outputPathParamRequired = true;
			requireDefaultPerf = true;
		}
		else if( argValue == "-gaze" )
		{
			gazeEnabled = true;
			requireDefaultPerf = true;
		}
		else if( argValue == "-help" )
		{
			printHelp = true;
		}
		else if( argValue == "-fullft" )
		{
			requireDefaultPerf = true;
		}
		else if( argValue == "-display" )
		{
			displayMainImage = true;
		}
		else
		{
			haveUnknownOption = true;
			unknownOptions += " " + argValue;
		}
		optind += 1;
	}

	if( requireDefaultPerf )
	{
		ftPerfMode = ECIFaceTrackerPerformanceMode::DEFAULT_PERF;
	}
	else if( requireMinimalPerf )
	{
		ftPerfMode = ECIFaceTrackerPerformanceMode::MINIMAL_PERF;
	}

	if( ( outputPathParamRequired && outputDataPath.empty() ) || haveUnknownOption || printHelp )
	{
		// unknown option or incorrect usage
		// print usage : 
		std::cout << std::endl << "Usages :" << std::endl;
		std::cout << "\tMaseDemo.exe [other options] : run MASE live using compatible connected webcam + given options enabled" << std::endl;
		std::cout << "\tMaseDemo.exe -sequence <path to sequence> [other options] : run MASE offline on sequence + given options enabled. REQUIRE '-output'" << std::endl;
		std::cout << "\t'-output' : define the folder where demo and MASE will output files" << std::endl;
		std::cout << "\t'-print' : display some basic tracking information to console" << std::endl;
		std::cout << "\t'-savelandmarks' : save frame landmarks in a file named 'frame_landmark.json'. REQUIRE '-output'" << std::endl;
		std::cout << "\t'-gaze' : run gaze detection feature. Activate full precision face tracker " << std::endl;
		std::cout << "\t'-fullft' : set the face tracker to always run in full precision mode" << std::endl;
		std::cout << "\t'-display' : display image" << std::endl;
		isGood = false;
		if( haveUnknownOption )
		{
			std::cout << "Unrecognized options : \"" << unknownOptions << "\""<< std::endl;
		}
		if( outputPathParamRequired && outputDataPath.empty() )
		{
			std::cout << "At least one used option requires '-output' to be defined." << std::endl;
		}
	}
	if( outputPathParamRequired && !fs::exists( outputDataPath ) )
	{
		std::cout << "\n\tThe given output folder must exist. Path "
			<< outputDataPath << " does not exist" << std::endl << std::endl;
		isGood = false;
	}

	// mase has to be created so we can have proper log library init and clean exit
	if( CreateMase( APP_NAME, true ) == ECIErrorCode::SUCCESS )
	{
		if( isGood )
		{
			MaseDataHandler *maseDH = MaseDataHandler::GetInstance();
			maseDH->EnableDataRecording( doRecord );
			maseDH->EnableEngineDataPrint( doPrint );
			maseDH->EnableDisplayImage( displayMainImage );
			if( !outputDataPath.empty() )
			{
				maseDH->SetOutputDataPath( outputDataPath );
			}
			maseDH->AttachToCallbacks();
			SetRunGazeEstimation( gazeEnabled );
			SetFaceTrackingPerformanceMode( ftPerfMode );

			if( !( inputSequencePath.empty() ) )
			{
				std::cout << "\n\tStarting offline with sequence : " << inputSequencePath << std::endl << std::endl;
				using convert_t = std::codecvt_utf8<wchar_t>;
				std::wstring_convert<convert_t, wchar_t> strconverter;
				std::wstring wImLoad = strconverter.from_bytes( inputSequencePath );
				std::wstring wImResult = strconverter.from_bytes( outputDataPath );
				std::wstring wCalibFile;
				std::wstring wImResultOverrideName;

				SetMaseOfflineParameters( wImLoad.c_str(), wImResult.c_str(), wCalibFile.c_str(), wImResultOverrideName.c_str() );

				isGood &= RunAndWaitMase();
				// dump to same folder as Mase result file
				maseDH->WriteLandmarksToJsonFile();
			}
			else
			{
				std::cout << "\n\tStarting online, will try to connect to compatible cameras." << std::endl << std::endl;
				isGood &= RunAndWaitMase();
				// dump to current working directory
				maseDH->WriteLandmarksToJsonFile();
			}
			if( ShutdownMase( 0 ) != ECIErrorCode::SUCCESS )
			{
				std::cout << "\n\tError when closing engine " << std::endl;
			}
		}
		MaseDataHandler::CleanInstance();
	}
	Cleanup();
	return isGood != true;
}
